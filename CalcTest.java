package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;

public class CalcTest {
    Calc calc;

    @BeforeAll
    public static void init(){

    }

    @BeforeEach
    public void setup(){
        calc = new Calc();
    }

    @Test
    public void Add_TwoPlusFour_Six(){
        int a = 2;
        int b = 4;
        int expectedResult = 6;

        int result = calc.add(a,b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Subtract_SevenMinusTwo_Five(){
        int a = 7;
        int b = 2;
        int expectedResult = 5;

        int result = calc.subtract(a,b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Multiply_ThreeTimesTwo_Six(){
        int a = 3;
        int b = 2;
        int expectedResult = 6;

        int result = calc.multiply(a,b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Divide_EightDividedFour_Two(){
        int a = 8;
        int b = 4;
        int expectedResult = 2;

        int result = calc.divide(a,b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Divide_TwoDividedByZero_ExceptionThrow(){
        int a = 2;
        int b = 0;

//        int result = calc.divide(a,b);

        Assertions.assertThrows(Exception.class,() -> calc.divide(a,b));
    }

    @AfterEach
    public void closeEach(){

    }

    @AfterAll
    public static void closeAll(){

    }
}
